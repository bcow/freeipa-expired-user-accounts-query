# Utility for FreeIPA user password expiration query

Command line utility to query FreeIPA (or other) LDAP directory for expired or expiring user accounts.

External script can be executed if matches are found.