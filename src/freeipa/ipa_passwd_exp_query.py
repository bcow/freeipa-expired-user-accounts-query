#!/usr/bin/env python
# encoding: utf-8
'''
 freeipa.ipa-passwd-exp-query -- Command line utility to query FreeIPA (or other) LDAP directory for expired or expiring user accounts.

 Command line utility to query FreeIPA (or other) LDAP directory for expired or expiring user accounts.

It defines CLIError and main

@author:     Antti Peltonen

@copyright:  2013 Antti Peltonen. All rights reserved.

@license:    GPLv3

@contact:    antti.peltonen@iki.fi
@deffield    updated: Updated
'''

import sys
import os
import datetime
import subprocess

import ldap #@UnusedImport
import ldap.sasl

from distutils import version

from ldap.controls import SimplePagedResultsControl

from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter

__all__ = []
__version__ = "0.3.2"
__date__ = '2013-11-06'
__updated__ = '2013-11-08'

class CLIError(Exception):
    '''Generic exception to raise and log different fatal errors.'''
    def __init__(self, msg):
        super(CLIError).__init__(type(self))
        self.msg = "E: %s" % msg
    def __str__(self):
        return self.msg
    def __unicode__(self):
        return self.msg

def main(argv=None): # IGNORE:C0111
    '''Command line options.'''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_version = "v%s" % __version__
    program_build_date = str(__updated__)
    program_version_message = '%%(prog)s %s (%s)' % (program_version, program_build_date)
    program_shortdesc = "Command line utility to query FreeIPA (or other) LDAP directory for expired or expiring user accounts."
    program_license = '''%s

  ipa_passwd_exp_query Copyright (C) 2013 Antti Peltonen
  This program comes with ABSOLUTELY NO WARRANTY.
  This is free software, and you are welcome to redistribute it
  under certain conditions.

  Licensed under the GNU GENERAL PUBLIC LICENSE version 3.
  https://www.gnu.org/licenses/gpl-3.0.txt


  Created by Antti Peltonen <antti.peltonen@iki.fi> on %s.

USAGE
''' % (program_shortdesc, str(__date__))

    try:
        # Setup argument parser
        parser = ArgumentParser(description=program_license, formatter_class=RawDescriptionHelpFormatter)
        parser.add_argument("-g", "--gssapi", dest="gssapi", action="store_true", help="use GSSAPI authentication when connecting to LDAP: %(default)s]")
        parser.add_argument("-b", "--binddn", dest="binddn", help="bind DN to use for authenticating into LDAP [default: %(default)s]. Ignored if -g/--gssapi is used.")
        parser.add_argument("-i", "--bindpw", dest="bindpw", help="password for bind dn [default: %(default)s]. Ignored if -g/--gssapi is used.")
        parser.add_argument("-t", "--tls", dest="tls", action="store_true", help="Use TLS to secure communication channel [default: %(default)s]. Ignored if -g/--gssapi is used.")
        parser.add_argument("-l", "--ssl", dest="ssl", action="store_true", help="Use SSL to secure communication channel [default: %(default)s]. Ignored if -g/--gssapi or -t/--tls is used.")
        parser.add_argument("-c", "--cert", dest="cert", help="Optional client certificate file for TLS and SSL connections [default: %(default)s]. Ignored if -g/--gssapi is used.")
        parser.add_argument("-k", "--key", dest="key", help="Optional client key file for TLS and SSL connections [default: %(default)s]. Ignored if -g/--gssapi is used.")
        parser.add_argument("-r", "--cacert", dest="cacert", help="Optional certificate authority certificate file for TLS and SSL connections [default: %(default)s]. Ignored if -g/--gssapi is used.")
        parser.add_argument("-v", "--verbose", dest="verbose", action="count", help="set verbosity level [default: %(default)s]")
        parser.add_argument("-D", "--basedn", required=True, dest="basedn", help="base DN with user accounts to review [default: %(default)s]")
        parser.add_argument("-s", "--server", dest="server", default="localhost", help="LDAP server to connect to [default: %(default)s]")
        parser.add_argument("-p", "--port", dest="port", default="389", help="LDAP server port to connect to [default: %(default)s]")
        parser.add_argument("-a", "--script", dest="script", help="Script to execute for each matching user. Script receives three arguments: \
        name, email, zulu [default: %(default)s]")
        parser.add_argument("-d", "--days", dest="days", default="0", help="Users password expiring before days in the future [default: %(default)s]")
        parser.add_argument("-V", "--version", action="version", version=program_version_message)

        # Process arguments
        args = parser.parse_args()

        gssapi = args.gssapi
        binddn = args.binddn
        bindpw = args.bindpw
        tls = args.tls
        ssl = args.ssl
        cert = args.cert
        key = args.key
        cacert = args.cacert
        verbose = args.verbose
        server = args.server
        port = args.port
        basedn = args.basedn
        days = args.days
        script = args.script
        
        zulu = (datetime.datetime.today() + datetime.timedelta(int(days))).strftime("%Y%m%d%H%M%SZ")
        if verbose >= 2:
            print "D: Calculated time for query is {zulu}".format(zulu=zulu)

        ldap_conn_done = False
        ldap_proto = "ldap"
        if ssl and not gssapi and not tls:
            ldap_proto = "ldaps"
        if cacert:
            ldap.set_option(ldap.OPT_X_TLS_CACERTFILE, cacert)
        if cert:
            ldap.set_option(ldap.OPT_X_TLS_CERTFILE, cert)
        if key:
            ldap.set_option(ldap.OPT_X_TLS_KEYFILE, key)

        conn_str= '{proto}://{server}:{port}'.format(proto=ldap_proto,server=server, port=port)
        ldap_conn = ldap.initialize(conn_str)
        ldap_conn.protocol_version = 3

        if verbose >= 2:
            print "D: Connecting to {str}".format(str=conn_str)

        if gssapi:
            if verbose >= 2:
                print "D: Trying GSSAPI authentication"
            auth_tokens = ldap.sasl.gssapi()
            ldap_conn.sasl_interactive_bind_s('', auth_tokens)
            ldap_conn_done = True

        if not ldap_conn_done and binddn:
            if tls:
                if verbose >= 2:
                    print "D: Attempting secure connection"
                ldap_conn.start_tls_s()
            if verbose >= 2:
                print "D: Trying simple bind authentication"
            ldap_conn.simple_bind(binddn, bindpw)
            ldap_conn_done = True

        if not ldap_conn_done:
            raise CLIError("No login information given. Unable to login to LDAP server.")
            return 1

        if version.StrictVersion('2.4.0') <= version.StrictVersion(ldap.__version__):
            LDAP_CONTROL_PAGED_RESULTS = ldap.CONTROL_PAGEDRESULTS #@UndefinedVariable
        else:
            LDAP_CONTROL_PAGED_RESULTS = ldap.LDAP_CONTROL_PAGE_OID #@UndefinedVariable

        required = True
        page_size = 100
        if version.StrictVersion('2.4.0') <= version.StrictVersion(ldap.__version__):
            req_ctrl = SimplePagedResultsControl(required,size=page_size,cookie='')
        else:
            req_ctrl = SimplePagedResultsControl(LDAP_CONTROL_PAGED_RESULTS, required, (page_size, ''))
        
        while True:
            msgid = ldap_conn.search_ext(basedn,
                                         ldap.SCOPE_ONELEVEL,
                                         '(&(krbPasswordExpiration<={zulu})(objectClass=posixAccount)(objectClass=krbPrincipalAux))'.format(zulu=zulu),
                                         ['cn', 'mail', 'displayName', 'krbPasswordExpiration'], serverctrls=[req_ctrl])
            rtype, rdata, rmsgid, serverctrls = ldap_conn.result3(msgid) #@UnusedVariable
            
            for (dn, attrs) in rdata:
                if attrs.has_key('mail'):
                    email = attrs['mail'][0]
                else:
                    email = None
                if attrs.has_key('displayName'):
                    name = attrs['displayName'][0]
                else:
                    name = attrs['cn'][0]
                expiration_date = attrs['krbPasswordExpiration'][0]
                if verbose >= 2:
                    print "D: {dn}".format(dn=dn)
                if verbose >= 1:
                    info_line = "\"{name}\" <{email}> {date}".format(name=name, email=email, date=expiration_date)
                    print "I: {info}".format(info=info_line)
                if script:
                    if verbose >= 2:
                        print "D: Executing script {script}".format(script=script)
                    subprocess.call([str(script), str(name), str(email), str(expiration_date)])
                    
            pctrls = [
                      c
                      for c in serverctrls
                      if c.controlType == SimplePagedResultsControl.controlType
                      ]

            if pctrls:
                if version.StrictVersion('2.4.0') <= version.StrictVersion(ldap.__version__):
                    if pctrls[0].cookie:
                        req_ctrl.cookie = pctrls[0].cookie
                    else:
                        break
                else:
                    if pctrls[0].controlValue[1]:
                        req_ctrl.cookie = pctrls[0].controlValue[1]
                    else:
                        break

        return 0
    except KeyboardInterrupt:
        ### handle keyboard interrupt ###
        return 0
    except Exception, e:
        raise(e)
        indent = len(program_name) * " "
        sys.stderr.write(program_name + ": " + repr(e) + "\n")
        sys.stderr.write(indent + "  for help use --help")
        return 2

if __name__ == "__main__":
    sys.exit(main())