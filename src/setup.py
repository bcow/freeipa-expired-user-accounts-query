#!/usr/bin/env python
# encoding: utf-8

import os
from setuptools import setup

# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "ipa-passwd-exp-query",
    version = "0.3.2",
    author = "Antti Peltonen",
    author_email = "antti.peltonen@patentpending.fi",
    description = ("Command line utility to query FreeIPA LDAP server for expiring user accounts"),
    license = "Apache-2.0",
    keywords = "freeipa ipa password",
    url = "https://bitbucket.org/bcow/freeipa-expired-user-accounts-query",
    packages=['freeipa'],
    long_description=read('README'),
    classifiers=[
        "Development Status :: 4 - Production/Beta",
        "Topic :: Security",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
    ],
    entry_points={'console_scripts': ['ipa-passwd-exp-query = freeipa.ipa_passwd_exp_query:main']},
    install_requires=['python-ldap>=2.0,<3.0',
                      'argparse>=1.2,<2.0',],
    data_files = [],
)